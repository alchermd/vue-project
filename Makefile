build:
	docker build -t yourname/vue-project .

start:
	docker run -it --rm -v ${PWD}:/app -v /app/node_modules -p 8080:8080 yourname/vue-project
